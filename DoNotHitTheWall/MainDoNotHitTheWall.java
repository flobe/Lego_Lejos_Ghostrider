package DoNotHitTheWall;

import GeneralClasses.ExchangeObject;
import GeneralClasses.MoveAround;
import GeneralClasses.CameraAction;

public class MainDoNotHitTheWall {
	
	public static void main(String [] args) {
		
		//initialising the objects and variables
		ExchangeObject ex = new ExchangeObject();
		MoveAround moveAround = new MoveAround(ex);
		DetectObstacle detObs = new DetectObstacle(ex, 0.20);
		CameraAction ca = new CameraAction();
		
		//starting the Threads
		moveAround.start();
		detObs.start();
		ca.start();
		
	}
	
}