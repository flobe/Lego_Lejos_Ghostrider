package DoNotHitTheWall;

import GeneralClasses.ExchangeObject;
import lejos.hardware.ev3.LocalEV3;
import lejos.hardware.port.Port;
import lejos.hardware.sensor.EV3UltrasonicSensor;
import lejos.robotics.SampleProvider;

public class DetectObstacle extends Thread {

	//initialisation of the objects and variables
	private ExchangeObject exObj;
	private SampleProvider sp;
	private float[] sample;
	private float minDist;
	private EV3UltrasonicSensor ultrasonicSensor;
	
	//constructor
	public DetectObstacle(ExchangeObject ex, double minDist) {
		this.exObj = ex;
		this.exObj.setMove(true);
		this.exObj.setObstacle(false);
		
		Port sensorPort = LocalEV3.get().getPort("S1");
		ultrasonicSensor = new EV3UltrasonicSensor(sensorPort);
		
		sp = ultrasonicSensor.getDistanceMode();
		sample = new float[1];
		
		this.minDist = (float)minDist;
	}
	
	@Override
	public void run() {
		
		while (exObj.getExecute()) {
			sleepThread(250);
			sp.fetchSample(sample, 0);
			if (sample[0] >= minDist) {
				
				exObj.setMove(true);
				exObj.setDistance((double)sample[0]);
				exObj.setObstacle(false);
				
			} else {
				
				exObj.setObstacle(true);
				exObj.setMove(false);
				exObj.setDistance((double)sample[0]);
			}
			
		}
		ultrasonicSensor.close();
		System.exit(0);
	}
	
	
	private void sleepThread(long ms) {
		try {
			Thread.sleep(ms);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
