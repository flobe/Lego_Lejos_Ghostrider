package RescueSound;

import DoNotHitTheWall.DetectObstacle;
import GeneralClasses.CameraAction;
import GeneralClasses.ConnectBluetooth;
import GeneralClasses.ExchangeObject;

public class MainRescueBot {

	
	public static void main(String[] args) {
		
		//initialize objects
		ExchangeObject ex = new ExchangeObject();
		ConnectBluetooth connBlue = new ConnectBluetooth();
		RescueBot rescBot = new RescueBot(ex, connBlue);
		DetectObstacle checkObs = new DetectObstacle(ex, 0.2);
		CameraAction cam = new CameraAction();
		
		//start the Threads
		cam.start();
		rescBot.start();
		checkObs.start();
		//camera
		
	}
}
