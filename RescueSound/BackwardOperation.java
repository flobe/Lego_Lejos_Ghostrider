package RescueSound;

import java.util.Random;

import GeneralClasses.ExchangeObject;
import GeneralClasses.Machine;
import lejos.hardware.ev3.LocalEV3;
import lejos.hardware.motor.EV3LargeRegulatedMotor;
import lejos.hardware.port.Port;
import lejos.robotics.navigation.MovePilot;

public class BackwardOperation extends Thread {
	
	//initialising the variables
		private double degrees;
		
		//initialising the objects
		private ExchangeObject exObj;
		private MovePilot pilot;
		private EV3LargeRegulatedMotor headTurn;
		
		//constructor
		public BackwardOperation(ExchangeObject ex) {
			this.exObj = ex;
			this.exObj.setUnderground(true);
			
			Machine machine = new Machine();
			pilot = machine.getPilot();
			
			Port motorPort = LocalEV3.get().getPort("A");
			headTurn = new EV3LargeRegulatedMotor(motorPort);
			
		}
		
		@Override
		public void run() {
			
			while (exObj.getExecute()) {
				
				if(!exObj.getMove()) {
					pilot.stop();
					
					if (exObj.getObstacle()) {
						
						headTurn.rotate(35);
						degrees = exObj.getDistance();
						headTurn.rotate(-70);
						if (degrees < exObj.getDistance()) {
							pilot.travel(-150);
							pilot.rotate(randomNumber(136));
						} else {
							pilot.travel(-150);
							pilot.rotate(-(randomNumber(136)));
						}
						headTurn.rotate(35);
						
					} else if (!exObj.getUnderground()) {
						pilot.travel(-150);
						if (randomInteger() > 0) {
							pilot.rotate(randomNumber(150));
						} else {
							pilot.rotate(-(randomNumber(150)));
						}
					}
					
				}
				
			}
		}
		
		//45-135
		
		//method for creating random numbers
		private double randomNumber(int num) {
			
			Random random = new Random();
			double value = 0;
			while (value > 136 || value < 40) {
				value = (double)random.nextInt(num);
			}
			return value;
			
		}
		
		private int randomInteger() {
			Random random = new Random();
			return random.nextInt(1);
		}
	}
