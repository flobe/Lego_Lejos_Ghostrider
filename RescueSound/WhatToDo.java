package RescueSound;

import GeneralClasses.ExchangeObject;
import DoNotFallDown.DetectFloor;
import DoNotHitTheWall.DetectObstacle;
import RescueSound.BackwardOperation;

public class WhatToDo extends Thread {

	//Variables
	
	//Objects
	private ExchangeObject exObj;
	private DetectFloor checkFloor;
	private DetectObstacle checkFront;
	private RescueBot rescBot;

	//constructor
	WhatToDo(ExchangeObject e, RescueBot bot) {
		this.exObj = e;
		this.rescBot = bot;
	}
	@Override
	public void run() {
		while(exObj.getExecute()) {
			if (exObj.getObstacle()) {
				stopRescueRobot();
			} else if (exObj.getUnderground()) {
				stopRescueRobot();
			} else if (exObj.getMove()) {
				startRescueRobot();
			}
		}
	}
	
	private synchronized void stopRescueRobot() {
		synchronized(rescBot) {
			try {
				rescBot.wait();
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
	
	private synchronized void startRescueRobot() {
		synchronized(rescBot) {
			rescBot.notify();
		}
	}
}
