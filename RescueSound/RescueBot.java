

package RescueSound;

import java.util.Random;

import GeneralClasses.Arm;
import GeneralClasses.ConnectBluetooth;
import GeneralClasses.ExchangeObject;
import GeneralClasses.Machine;

import lejos.hardware.Button;
import lejos.hardware.ev3.LocalEV3;
import lejos.hardware.lcd.LCD;
import lejos.hardware.motor.EV3LargeRegulatedMotor;
import lejos.hardware.port.Port;
import lejos.hardware.sensor.EV3ColorSensor;
import lejos.hardware.sensor.NXTSoundSensor;
import lejos.robotics.Color;
import lejos.robotics.SampleProvider;
import lejos.robotics.navigation.MovePilot;

public class RescueBot extends Thread {
	
	//initializing variables
	private double degrees;
	
	//initializing objects
	private ExchangeObject exObj;
	private NXTSoundSensor soundSensor1;
	private NXTSoundSensor soundSensor2;
	private NXTSoundSensor soundSensor3;
	private float[] sample1;
	private float[] sample2;
	private float[] sample3;
	private float[] colorSample;
	private SampleProvider colorProvider;
	private EV3ColorSensor colorSensor;
	private SampleProvider sp1;
	private SampleProvider sp2;
	private SampleProvider sp3;
	private MovePilot pilot;
	private EV3LargeRegulatedMotor headTurn;
	private Machine m;
	private ConnectBluetooth bluetoothRobot;
	private Arm arm;
	//constructor
	public RescueBot(ExchangeObject ex, ConnectBluetooth bluetoothRobot) {
		this.exObj = ex;
		this.bluetoothRobot = bluetoothRobot;
		try {
	        //Motoren und Sensoren Lokal aktivieren
	        m = new Machine();
	        pilot = m.getPilot();
	        Port motorPort = LocalEV3.get().getPort("A");
	        headTurn = new EV3LargeRegulatedMotor(motorPort);
	        
	        Port s2 = LocalEV3.get().getPort("S2");
			colorSensor = new EV3ColorSensor(s2);
			colorProvider = colorSensor.getColorIDMode();
			colorSample = new float[colorProvider.sampleSize()];
			
			//activated bluetooth and the sensors
			bluetoothRobot.startSoundSensor();
	        soundSensor1 = bluetoothRobot.getSoundSensor("soundSensor1");
	        sp1 = soundSensor1.getDBMode();
	        soundSensor2 = bluetoothRobot.getSoundSensor("soundSensor2");
	        sp2 = soundSensor2.getDBMode();
	        soundSensor3 = bluetoothRobot.getSoundSensor("soundSensor3");
	        sp3 = soundSensor3.getDBMode();
	        sample1 = new float[3];
	        sample2 = new float[3];
	        sample3 = new float[3];
	        
	        arm = new Arm(bluetoothRobot);
		} catch (Exception ex1) {
			System.out.println(ex1);
		}
	}
	
	@Override
	public void run () {
		try {
	        
	        //Hauptteil: Schleife f�r Fahrrichtung
	        colorProvider.fetchSample(colorSample, 0);
			System.out.println(colorSample[0]);
			arm.calibrate();
			//boolean red = false;
			while (exObj.getExecute()){
				int currentcolor = colorSensor.getColorID();
				switch(currentcolor){
				case Color.RED:
					//red = true;
					
					LCD.drawString("Rot next Program now -> Press Button", 0, 5);
					Button.waitForAnyPress();
					LCD.clear();LCD.refresh();
					pilot.forward();
					while (exObj.getDistance() > 0.05) {
						
					}
					pilot.stop();
					
					arm.grabSomething();
					arm.checkColor();
					pilot.travel(-200);
					pilot.rotate(90);
					break;
				default:
					//red=false;
					
					//evtl wait einbauen wenn der Roboter/Motoren zu laut ist?
					//Delay.msDelay(100);
					sp1.fetchSample(sample1, 0); //Hinten
		        	sp2.fetchSample(sample2, 0); //Links
		        	sp3.fetchSample(sample3, 0); //Rechts
		        	System.out.println("Hinten :" + sample1[0]);
		    		System.out.println("Links   :" + sample2[0]);
		    		System.out.println("Rechts:" + sample3[0]);

			    				//kann man optimieren!!!
			    				//Winkel genau berechnen von wo der Laut herkommt!
			    	if(!exObj.getMove()) {
			    		goBack();
			    	} else 
			    	//f�ngt erst an wenn gewisses Soundniveau erreicht wird
			    	if (sample1[0]>0.35 || sample2[0]>0.35 || sample3[0]>0.35) {
						//in die Richtung zeigen von wo der Laut herkommt
						if (sample1[0] > sample2[0]+0.1 & sample1[0] > sample3[0]+0.1) { //also wenn Hinten am lautesten mit min Abstand 0,1
							//turn 180Grad (90Grad auf der Stelle entspricht -360/360)
							pilot.rotate(200);
							//in die Richtung bewegen
							pilot.travel(150);
						} else {
							if (sample2[0] > sample3[0]+0.15) { //also wenn links lauter als rechts ist
								//turn nach links
								pilot.rotate(40);
								//in die Richtung bewegen
								pilot.travel(150);
							} else if (sample3[0] > sample2[0]+0.15) { //also wenn rechts lauter als links ist
								//turn nach rechts
								pilot.rotate(-40);
								//in die Richtung bewegen
								pilot.travel(150);
							}
						}
			    	}
					break;
				}
			}


			
			
			//beenden aller Ger�te
			m.closeMotors();
	        colorSensor.close();
			soundSensor1.close();
			soundSensor2.close();
			soundSensor3.close();
		
		} catch (Exception ex) {
    		System.out.println(ex);
    	}
    }
	
	private void goBack() {
		pilot.stop();
		if (exObj.getObstacle()) {
					
			headTurn.rotate(35);
			degrees = exObj.getDistance();
			headTurn.rotate(-70);
			if (degrees < exObj.getDistance()) {
				pilot.travel(-150);
				pilot.rotate(randomNumber(150));
			} else {
				pilot.travel(-150);
				pilot.rotate(-(randomNumber(150)));
			}
			headTurn.rotate(35);
		}	
			
	}
	
	//method for creating random numbers
			private double randomNumber(int num) {
				
				Random random = new Random();
				double value = 0;
				while (value > 136 || value < 40) {
					value = (double)random.nextInt(num);
				}
				return value;
				
			}
	
}

