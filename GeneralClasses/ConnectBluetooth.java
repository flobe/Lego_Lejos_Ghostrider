package GeneralClasses;

import java.net.MalformedURLException;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;

import lejos.hardware.Brick;
import lejos.hardware.BrickFinder;
import lejos.hardware.port.Port;
import lejos.hardware.sensor.EV3ColorSensor;
import lejos.hardware.sensor.EV3TouchSensor;
import lejos.hardware.sensor.NXTSoundSensor;
import lejos.remote.ev3.RMIRegulatedMotor;
import lejos.remote.ev3.RemoteEV3;

public class ConnectBluetooth {

	
	//variables
	boolean soundActive;
	boolean motorActive;
	boolean touchActive;
	
	//objects
	private RMIRegulatedMotor remoteA;
	private RMIRegulatedMotor remoteB;
	private RMIRegulatedMotor remoteC;
	private RMIRegulatedMotor remoteD;
	private Brick[] bricks;
	private NXTSoundSensor soundSensor1;
	private NXTSoundSensor soundSensor2;
	private NXTSoundSensor soundSensor3;
	private EV3TouchSensor touchSensor;
	private RemoteEV3 ev2;
	
	
	//constructor
	public ConnectBluetooth() {
		
		try {
			
			String[] names = {"EV1", "EV2"};
	        bricks = new Brick[names.length];
	        bricks[0] = BrickFinder.getLocal();
	        System.out.println(bricks[0].getName());
	        System.out.println("Connect " + names[1]);
	        bricks[1] = new RemoteEV3(BrickFinder.find(names[1])[0].getIPAddress());
			
			ev2 = new RemoteEV3("10.0.1.3");
			
			soundActive = false;
			motorActive = false;
			touchActive = false;
			
		} catch (RemoteException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (NotBoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
	//methods
	public boolean getTouchActive() {
		return this.touchActive;
	}
	
	public boolean getMotorActive() {
		return this.motorActive;
	}
	
	public boolean getSoundActive() {
		return  this.soundActive;
	}
	
	public void startTouchSensor() {
		Port remoteS1 = bricks[1].getPort("S1");
		
		touchSensor = new EV3TouchSensor(remoteS1);
	}
	
	public void startSoundSensor() {
		Port remoteS2 = bricks[1].getPort("S2");
		Port remoteS3 = bricks[1].getPort("S3");
		Port remoteS4 = bricks[1].getPort("S4");
		
		soundSensor1 = new  NXTSoundSensor(remoteS2);
		soundSensor2 = new  NXTSoundSensor(remoteS3);
		soundSensor3 = new  NXTSoundSensor(remoteS4);
		
		soundSensor1.getDBMode();
		soundSensor2.getDBMode();
		soundSensor3.getDBMode();
		
		soundActive = true;
	}
	
	public void startMotors() {
		remoteA = ev2.createRegulatedMotor("A", 'M');
		remoteB = ev2.createRegulatedMotor("B", 'M');
		remoteC = ev2.createRegulatedMotor("C", 'L');
		remoteD = ev2.createRegulatedMotor("D", 'L');
		
		motorActive = true;
	}
	
	public EV3TouchSensor getTouchSensor() {
		return touchSensor;
	}
	
	public NXTSoundSensor getSoundSensor(String name) {
		switch (name) {
		case "soundSensor1":
			return soundSensor1;
		case "soundSensor2":
			return soundSensor2;
		case "soundSensor3":
			return soundSensor3;
		default: 
				return soundSensor1;
		}
	}
	
	public void closeSoundSensor(String name) {
		getSoundSensor(name).close();
		
		soundActive = false;
	}
	
	public RMIRegulatedMotor getMotor(String name) {
		switch (name) {
		case "remoteA":
			return remoteA;
		case "remoteB":
			return remoteB;
		case "remoteC":
			return remoteC;
		case "remoteD":
			return remoteD;
		default: 
			return remoteA;
		}
	}
	
	public void closeMotor(String name) {
		try {
			getMotor(name).close();
			
			motorActive = false;
		} catch (RemoteException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
