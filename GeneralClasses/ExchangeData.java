package GeneralClasses;

public interface ExchangeData {

	public boolean getMove();
	public void setMove(boolean move);
	
	public int getDirection();
	public void setDirection(int dir);
	
	public boolean getObstacle();
	public void setObstacle(boolean obs);
	
	public boolean getUnderground();
	public void setUnderground(boolean under);
	
	public boolean getExecute();
	
	public boolean getIsMoving();
	public void setIsMoving(boolean isMoving);
	
}
