package GeneralClasses;

import lejos.hardware.Button;

public class ExchangeObject {

	// local variables for saving values from the Threads
	private boolean move;
	private int direction;
	private boolean obstacle;
	private boolean underground;
	private boolean isMoving;
	private double distance;
	
	//methods used to pass the values between different Threads
	public synchronized boolean getMove() {
		return this.move;
	}

	public synchronized void setMove(boolean move) {
		this.move = move;
	}
	
	public synchronized int getDirection() {
		return this.direction;
	}

	public synchronized void setDirection(int dir) {
		this.direction = dir;
	}

	public synchronized boolean getObstacle() {
		return this.obstacle;
	}

	public synchronized void setObstacle(boolean obs) {
		this.obstacle = obs;
	}

	public synchronized boolean getUnderground() {
		return this.underground;
	}

	public synchronized void setUnderground(boolean under) {
		this.underground = under;
	}

	public synchronized boolean getExecute() {
		return Button.ESCAPE.isUp();
	}

	public synchronized boolean getIsMoving() {
		return this.isMoving;
	}

	public synchronized void setIsMoving(boolean isMoving) {
		this.isMoving = isMoving;
	}

	public synchronized double getDistance() {
		return this.distance;
	}
	
	public synchronized void setDistance(double dist) {
		this.distance = dist;
	}

}
