package GeneralClasses;

import lejos.hardware.ev3.LocalEV3;
import lejos.hardware.motor.EV3LargeRegulatedMotor;
import lejos.robotics.chassis.Chassis;
import lejos.robotics.chassis.Wheel;
import lejos.robotics.chassis.WheeledChassis;
import lejos.robotics.navigation.MovePilot;

public class Machine {

	//initialising the objects
	private MovePilot pilot;
	private Chassis myChassis;
	EV3LargeRegulatedMotor leftMotorB;
	EV3LargeRegulatedMotor rightMotorC;
	//constructor
	public Machine() {
		leftMotorB = new EV3LargeRegulatedMotor(LocalEV3.get().getPort("B"));
		rightMotorC = new EV3LargeRegulatedMotor(LocalEV3.get().getPort("C"));
		Wheel leftWheel = WheeledChassis.modelWheel(leftMotorB, 56.0).offset(97.5);
		Wheel rightWheel = WheeledChassis.modelWheel(rightMotorC, 56.0).offset(-97.5);
		myChassis = new WheeledChassis( new Wheel[]{leftWheel, rightWheel}, WheeledChassis.TYPE_DIFFERENTIAL);
		myChassis.setAcceleration(100, 100);
		myChassis.setSpeed(150, 200);
		pilot = new MovePilot(myChassis);
		pilot.setLinearSpeed(100);
	}
	
	public void setVelocity(double linSpeed, double angSpeed) {
		this.myChassis.setVelocity(linSpeed, angSpeed);
	}
	
	public MovePilot getPilot() {
		return this.pilot;
	}
	
	public void closeMotors() {
		leftMotorB.close();
		rightMotorC.close();
	}
}
