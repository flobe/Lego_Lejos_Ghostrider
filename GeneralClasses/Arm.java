package GeneralClasses;

import java.rmi.RemoteException;

import GeneralClasses.ConnectBluetooth;
import lejos.hardware.Button;
import lejos.hardware.ev3.LocalEV3;
import lejos.hardware.lcd.LCD;
import lejos.hardware.port.Port;
import lejos.hardware.sensor.EV3ColorSensor;
import lejos.hardware.sensor.EV3TouchSensor;
import lejos.remote.ev3.RMIRegulatedMotor;
import lejos.robotics.Color;
import lejos.robotics.SampleProvider;
import lejos.utility.Delay;

public class Arm {

	
	//variables
	
	//objects
	private EV3ColorSensor colorSensor;
	private ConnectBluetooth connectBluetooth;
	private RMIRegulatedMotor remoteA;
	private RMIRegulatedMotor remoteB;
	private RMIRegulatedMotor remoteC;
	private RMIRegulatedMotor remoteD;
	private EV3TouchSensor touchSensor;
	
	//constructor
	public Arm(ConnectBluetooth connBlue) {
		this.connectBluetooth = connBlue;
		
		if (!connectBluetooth.getMotorActive()) {
			connectBluetooth.startMotors();
		}
		remoteA = connectBluetooth.getMotor("remoteA");
		remoteB = connectBluetooth.getMotor("remoteB");
		remoteC = connectBluetooth.getMotor("remoteC");
		remoteD = connectBluetooth.getMotor("remoteD");
		if (!connectBluetooth.getTouchActive()) {
			connectBluetooth.startTouchSensor();
		}
		touchSensor = connectBluetooth.getTouchSensor();
	}
	
	//methods
	
	public void up() {
		try {
			remoteC.rotate(-400);
		} catch (RemoteException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public void calibrate() {
		try {
			SampleProvider touchsample = touchSensor.getTouchMode();
	        float[] sample = new float[touchsample.sampleSize()];
	        remoteD.setSpeed(90); //das die Motoren nicht mit voller Kraft gegen hauen
			boolean limit = false;
			while (!limit) {
		    	remoteD.rotate(25);  //je kleiner dieser Wert umso genauer ist die Ausrichtung
		    	touchsample.fetchSample(sample, 0); 
		    	limit = (sample[0] == 1); 
		    }
			//Rechts X oder ca 280
		    //Vorne 0 oder X-308
		    //Links -280
		    //Hinten -540
			final int DMax = remoteD.getTachoCount(); 
			touchSensor.close(); 
			
		    remoteD.setSpeed(100);
		    remoteD.rotateTo(DMax-308); //X-308 Standardposition
			
		    
		    
		  //Solange bewegen bis VERTIKAL stop ist
		    remoteC.setSpeed(90);
		    remoteC.setStallThreshold(50,5); //(wie grob, wie lang)
		    while (!remoteC.isStalled()) {
		    	remoteC.rotate(-60);
		    }
		    //Y gibt den Startpunkt der horizontalen Bewegung an
		    //Unten ca105
		    //Oben ca-390
		    final int CMax = remoteC.getTachoCount(); 
		    remoteC.setSpeed(100);
		    remoteC.rotateTo(CMax+250); //Standardposition sollte oben bleiben
	
			
		    
		    
		    //Solange bewegen bis KLAUE geschlossen ist
			remoteA.setSpeed(90);
			while (!remoteA.isStalled()) {
				remoteA.rotate(20);  //Rotate Richtung noch austesten
			}
			//Z gibt den Startpunkt der horizontalen Bewegung an
			//Z-80 Offen
			//Z Geschlossen
			final int AMax = remoteA.getTachoCount(); 
			remoteA.setSpeed(100);
			remoteA.rotateTo(AMax-80); //Standardposition ist ge�ffnete Klaue
	
			    
	
			 //Mit Schwung nach Links und Rechts um dann in die Mitte zu fallen - Gravitation
		    remoteB.setSpeed(100);
		    remoteB.rotate(-180);
		    remoteB.flt(true); //schaltet Energie der Motoren aus ->Locker
		    Delay.msDelay(500);
		    remoteB.rotate(180);
		    remoteB.flt(true);
		    Delay.msDelay(500);
		    final int BMax = remoteB.getTachoCount();
		    remoteB.rotateTo(BMax); //Standardposition ist nach Unten
		} catch (Exception ex) {
			System.out.println(ex);
		}
	}
	
	public void grabSomething() {
		try {
			//C.rotate(80); //Klaue �ffnen		//Z wert des ArmKalib Programms mit einbinden
			remoteC.rotate(500); //down				//Y wert des ArmKalib Programms mit einbinden
			remoteA.rotate(-80); //Klaue schlie�en	//Z wert des ArmKalib Programms mit einbinden
			remoteC.rotate(-500); //up				//Y wert des ArmKalib Programms mit einbinden
		} catch (Exception ex) {
			System.out.println(ex);
		}
	}
	
	private void turnArm(boolean left) {
		try {
			if (left) {
				remoteD.rotate(-560);
			} else {
				remoteD.rotate(560);
			}
		} catch (RemoteException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
		
	public void checkColor() {
		try {
			turnArm(true);
			remoteC.rotate(60);
			Port sens3 = LocalEV3.get().getPort("S3");
			colorSensor = new EV3ColorSensor(sens3);
			LCD.clear();
			for (int k = 0; k < 7; k++) {
				System.out.println("");
			}
			while (Button.ENTER.isUp() & Button.ESCAPE.isUp() & Button.UP.isUp() & Button.DOWN.isUp() & Button.RIGHT.isUp() & Button.LEFT.isUp()) {
				int currentcolor = colorSensor.getColorID();
				switch(currentcolor){
				
				case Color.BLACK:
					LCD.drawString("Schwarz", 0, 5);
					Delay.msDelay(500);
					Button.waitForAnyPress();
					LCD.clear();LCD.refresh();
					break;		
				case Color.BLUE:
					LCD.drawString("Blau", 0, 5);
					Delay.msDelay(500);
					Button.waitForAnyPress();
					LCD.clear();LCD.refresh();
					break;
				case Color.BROWN:
					LCD.drawString("Braun", 0, 5);
					Delay.msDelay(500);
					Button.waitForAnyPress();
					LCD.clear();LCD.refresh();
					break;
				case Color.CYAN:
					LCD.drawString("Cyan", 0, 5);
					Delay.msDelay(500);
					Button.waitForAnyPress();
					LCD.clear();LCD.refresh();
					break;
				case Color.DARK_GRAY:
					LCD.drawString("Dunkel Grau", 0, 5);
					Delay.msDelay(500);
					Button.waitForAnyPress();
					LCD.clear();LCD.refresh();
					break;
				case Color.GRAY:
					LCD.drawString("Grau", 0, 5);
					Delay.msDelay(500);
					Button.waitForAnyPress();
					LCD.clear();LCD.refresh();
					break;
				case Color.GREEN:
					LCD.drawString("Gr�n", 0, 5);
					Delay.msDelay(500);
					Button.waitForAnyPress();
					LCD.clear();LCD.refresh();
					break;
				case Color.LIGHT_GRAY:
					LCD.drawString("Hell Grau", 0, 5);
					Delay.msDelay(500);
					Button.waitForAnyPress();
					LCD.clear();LCD.refresh();
					break;
				case Color.MAGENTA:
					LCD.drawString("Magenta", 0, 5);
					Delay.msDelay(500);
					Button.waitForAnyPress();
					LCD.clear();LCD.refresh();
					break;
				case Color.NONE:
					LCD.drawString("Keiner der Farben", 0, 5);
					Delay.msDelay(500);
					Button.waitForAnyPress();
					LCD.clear();LCD.refresh();
					break;
				case Color.ORANGE:
					LCD.drawString("Orange", 0, 5);
					Delay.msDelay(500);
					Button.waitForAnyPress();
					LCD.clear();LCD.refresh();
					break;
				case Color.PINK:
					LCD.drawString("Rosa", 0, 5);
					Delay.msDelay(500);
					Button.waitForAnyPress();
					LCD.clear();LCD.refresh();
					break;
				case Color.RED:
					LCD.drawString("Rot", 0, 5);
					Delay.msDelay(500);
					Button.waitForAnyPress();
					LCD.clear();LCD.refresh();
					break;
				case Color.WHITE:
					LCD.drawString("Wei�", 0, 5);
					Delay.msDelay(500);
					Button.waitForAnyPress();
					LCD.clear();LCD.refresh();
					break;
				case Color.YELLOW:
					LCD.drawString("Gelb", 0, 5);
					Delay.msDelay(500);
					Button.waitForAnyPress();
					LCD.clear();LCD.refresh();
					break;
				}
			}
			turnArm(false);
		} catch (Exception ex) {
			System.out.println(ex);
		}
	}
}
