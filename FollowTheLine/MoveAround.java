package FollowTheLine;

import GeneralClasses.ExchangeObject;
import GeneralClasses.Machine;
import lejos.hardware.ev3.LocalEV3;
import lejos.hardware.motor.EV3LargeRegulatedMotor;
import lejos.hardware.port.Port;
import lejos.robotics.navigation.MovePilot;

public class MoveAround extends Thread{

	
	//initialising variables
	private int degrees;
	
	//initialising objects
	private ExchangeObject exObj;
	private MovePilot pilot;
	private EV3LargeRegulatedMotor turnMotor;
	
	//constructor
	MoveAround(ExchangeObject ex) {
		this.exObj = ex;
		this.exObj.setIsMoving(false);
		this.exObj.setDirection(4);
		
		Machine ma = new Machine();
		pilot = ma.getPilot();
		
		Port motorPort = LocalEV3.get().getPort("A");
		turnMotor = new EV3LargeRegulatedMotor(motorPort);
	}
	
	//operation
	@Override
	public void run() {
		while (exObj.getExecute()) {
			degrees = 0;
			System.out.println(exObj.getDirection());
			if (exObj.getDirection() == 4) {
				System.out.println("warte");
			} else if (exObj.getDirection() == 0 & !exObj.getIsMoving()) {
				pilot.forward();
				exObj.setIsMoving(true);
				System.out.println("Move");
			} else if(exObj.getDirection() == 1) {
				if (exObj.getIsMoving()) {
					pilot.stop();
					exObj.setIsMoving(false);
				}
				while (exObj.getDirection() == 1) {
					degrees ++ ;
					turnMotor.rotate(1);
				}
				turnMotor.rotate(-degrees);
				pilot.rotate(degrees); 							//rotate left
			} else if(exObj.getDirection() == 2) {
				if (exObj.getIsMoving()) {
					pilot.stop();
					exObj.setIsMoving(false);
				}
				while (exObj.getDirection() == 2) {
					degrees ++;
					turnMotor.rotate(-1);						//rotate right
				}
				turnMotor.rotate(degrees);
				pilot.rotate(degrees);
			}
		}
	}
	
}
