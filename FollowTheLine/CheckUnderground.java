package FollowTheLine;

import GeneralClasses.ExchangeObject;
import lejos.hardware.Button;
import lejos.hardware.ev3.LocalEV3;
import lejos.hardware.port.Port;
import lejos.hardware.sensor.EV3ColorSensor;
import lejos.robotics.SampleProvider;

public class CheckUnderground extends Thread{

	
	//initialise variables
	private float[] sample;
	private double BLACK;
	private double WHITE;
	private double midValue;
	
	//initialise objects
	private SampleProvider sp;
	private ExchangeObject exObj;
	private EV3ColorSensor cs;
	//constructor
	CheckUnderground(ExchangeObject ex) {
		this.exObj = ex;
		this.exObj.setDirection(4);
		
		Port sensorPort = LocalEV3.get().getPort("S2");
		cs = new EV3ColorSensor(sensorPort);
		sp = cs.getRedMode();
		
		sample = new float[1];
		
		this.BLACK = calibrate("BLACK");
		this.WHITE = calibrate("WHITE");
		System.out.println("waiting for applying");
		Button.waitForAnyPress();
		
		this.midValue = (WHITE - BLACK) / 2 + BLACK;
		
	}
	
	//operation
	@Override
	public void run() {
		while (exObj.getExecute()) {
			sp.fetchSample(sample, 0);
			
			if (sample[0] < midValue - 0.07) {
				exObj.setDirection(1);	//links
			} else if (sample[0] > midValue + 0.07) {
				exObj.setDirection(2);	//rechts
			} else {
				exObj.setDirection(0);
			}
		}
		cs.close();
	}
	
	private double calibrate(String s) {
		System.out.println("Calibrate " + s);
		Button.waitForAnyPress();
		sp.fetchSample(sample, 0);
		return (double)sample[0];
	}
}
