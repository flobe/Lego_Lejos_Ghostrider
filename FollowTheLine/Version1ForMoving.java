package FollowTheLine;

import GeneralClasses.ExchangeObject;
import GeneralClasses.Machine;
import lejos.hardware.Button;
import lejos.hardware.ev3.LocalEV3;
import lejos.hardware.port.Port;
import lejos.hardware.sensor.EV3ColorSensor;
import lejos.robotics.SampleProvider;
import lejos.robotics.navigation.MovePilot;

public class Version1ForMoving extends Thread {
	
	//initialising variables
	private float[] sample;
	private double BLACK;
	private double WHITE;
	private double midValue;
	private double ki;
	private double kp;
	private double kd;
	private double correction;
	private double error;
	private double integral;
	private double derivative;
	private double lasterror;
	
	//initialising objects
	private ExchangeObject exObj;
	private SampleProvider sp;
	private EV3ColorSensor colorSensor;
	private MovePilot pilot;
	//constructor
	Version1ForMoving(ExchangeObject ex) {
		this.exObj = ex;
		this.exObj.setMove(true);
		
		Port secondSensorPort = LocalEV3.get().getPort("S2");
		colorSensor = new EV3ColorSensor(secondSensorPort);
		
		Machine m = new Machine();
		pilot = m.getPilot();
		
		this.sp = colorSensor.getMode("Red");
		this.sample = new float[1];
		
		calibrate();
		
		this.midValue = (WHITE - BLACK) / 2 + BLACK;
		kp = 1;
		ki = 1;
		kd = 1;
		lasterror = 0;
	}
	
	private void calibrate() {
		System.out.println("Calibrate Black-Value");
		Button.waitForAnyPress();
		sp.fetchSample(sample, 0);
		System.out.println("Calibrate White-Value");
		Button.waitForAnyPress();
		this.BLACK = (double)sample[0];
		sp.fetchSample(sample, 0);
		this.WHITE = (double)sample[0];
	}
	
	
	//function
	@Override
	public void run() {
		
		sp.fetchSample(sample, 0);
		
		error = midValue - sample[0];
		integral = error + integral;
		derivative = error - lasterror;
		
		correction = kp * error + ki * integral + kd * derivative;
		
		//turn robot by correction
		pilot.rotate(correction);
		
		pilot.forward();
		
		lasterror = error;
	}
	
	
	private void threadSleep(final int ms) {
		try {
			Thread.sleep(ms);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
	
}
