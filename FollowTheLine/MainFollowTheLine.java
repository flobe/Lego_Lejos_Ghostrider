package FollowTheLine;

import GeneralClasses.ExchangeObject;

public class MainFollowTheLine {

	
	public static void main(String [] args) {
		
		//initialising the necessary objects
		ExchangeObject ex = new ExchangeObject();
		MoveAround moveAroundThread = new MoveAround(ex);
		CheckUnderground checkUndergroundThread = new CheckUnderground(ex);
		
		//starting the Threads
		checkUndergroundThread.start();
		moveAroundThread.start();
	}
}
