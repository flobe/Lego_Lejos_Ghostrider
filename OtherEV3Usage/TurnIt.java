package OtherEV3Usage;

import lejos.hardware.Button;
import lejos.hardware.ev3.LocalEV3;
import lejos.hardware.lcd.LCD;
import lejos.hardware.port.Port;
import lejos.hardware.sensor.EV3ColorSensor;
import lejos.remote.ev3.RMIRegulatedMotor;
import lejos.remote.ev3.RemoteEV3;
import lejos.robotics.Color;
import lejos.robotics.SampleProvider;
import lejos.utility.Delay;

public class TurnIt {
	
	
	EV3ColorSensor colorSensor;
	SampleProvider colorProvider;
	float[] colorSample;
	
	public void aDrehen() {
		try {
		Port s2 = LocalEV3.get().getPort("S4"); //S3 des rechten EV3
		colorSensor = new EV3ColorSensor(s2);
		colorProvider = colorSensor.getColorIDMode();
		colorSample = new float[colorProvider.sampleSize()];
		
		RemoteEV3 ev2 = new RemoteEV3("10.0.1.3");
		
		RMIRegulatedMotor  remoteA = ev2.createRegulatedMotor("A",'M');
		RMIRegulatedMotor  remoteB = ev2.createRegulatedMotor("B",'M');
		RMIRegulatedMotor  remoteC = ev2.createRegulatedMotor("C",'L');
		RMIRegulatedMotor  remoteD = ev2.createRegulatedMotor("D",'L');
		
		//Damit der Arm nicht locker ist
		remoteA.rotate(-80); //Klaue schlie�en
		//final int Y = C.getTachoCount(); 
		//C.setSpeed(100);
		//C.rotateTo(Y+250); //Standardposition sollte oben bleiben
		//final int W = B.getTachoCount(); //ABER W ist immer die allererste Startposition... egal was!
	    //B.rotateTo(W); //Standardposition ist nach Unten
		// Vertikal sackt ab... Rotation �bersteuert....
		
		while (Button.ESCAPE.isUp()){
			remoteD.rotate(-540); //Standardposition-180 (X-308)-180 ist Hinten 
			
			int currentcolor = colorSensor.getColorID();
			switch(currentcolor){
			
			case Color.BLACK:
				LCD.drawString("Schwarz", 0, 5);
				Delay.msDelay(500);
				Button.waitForAnyPress();
				LCD.clear();LCD.refresh();
				break;		
			case Color.BLUE:
				LCD.drawString("Blau", 0, 5);
				Delay.msDelay(500);
				Button.waitForAnyPress();
				LCD.clear();LCD.refresh();
				break;
			case Color.BROWN:
				LCD.drawString("Braun", 0, 5);
				Delay.msDelay(500);
				Button.waitForAnyPress();
				LCD.clear();LCD.refresh();
				break;
			case Color.CYAN:
				LCD.drawString("Cyan", 0, 5);
				Delay.msDelay(500);
				Button.waitForAnyPress();
				LCD.clear();LCD.refresh();
				break;
			case Color.DARK_GRAY:
				LCD.drawString("Dunkel Grau", 0, 5);
				Delay.msDelay(500);
				Button.waitForAnyPress();
				LCD.clear();LCD.refresh();
				break;
			case Color.GRAY:
				LCD.drawString("Grau", 0, 5);
				Delay.msDelay(500);
				Button.waitForAnyPress();
				LCD.clear();LCD.refresh();
				break;
			case Color.GREEN:
				LCD.drawString("Gr�n", 0, 5);
				Delay.msDelay(500);
				Button.waitForAnyPress();
				LCD.clear();LCD.refresh();
				break;
			case Color.LIGHT_GRAY:
				LCD.drawString("Hell Grau", 0, 5);
				Delay.msDelay(500);
				Button.waitForAnyPress();
				LCD.clear();LCD.refresh();
				break;
			case Color.MAGENTA:
				LCD.drawString("Magenta", 0, 5);
				Delay.msDelay(500);
				Button.waitForAnyPress();
				LCD.clear();LCD.refresh();
				break;
			case Color.NONE:
				LCD.drawString("Keiner der Farben", 0, 5);
				Delay.msDelay(500);
				Button.waitForAnyPress();
				LCD.clear();LCD.refresh();
				break;
			case Color.ORANGE:
				LCD.drawString("Orange", 0, 5);
				Delay.msDelay(500);
				Button.waitForAnyPress();
				LCD.clear();LCD.refresh();
				break;
			case Color.PINK:
				LCD.drawString("Rosa", 0, 5);
				Delay.msDelay(500);
				Button.waitForAnyPress();
				LCD.clear();LCD.refresh();
				break;
			case Color.RED:
				LCD.drawString("Rot", 0, 5);
				Delay.msDelay(500);
				Button.waitForAnyPress();
				LCD.clear();LCD.refresh();
				break;
			case Color.WHITE:
				LCD.drawString("Wei�", 0, 5);
				Delay.msDelay(500);
				Button.waitForAnyPress();
				LCD.clear();LCD.refresh();
				break;
			case Color.YELLOW:
				LCD.drawString("Gelb", 0, 5);
				Delay.msDelay(500);
				Button.waitForAnyPress();
				LCD.clear();LCD.refresh();
				break;
			}
			remoteD.rotate(540);
		}
		remoteD.close();
		remoteC.close();
		remoteA.close();
		remoteB.close();
		} catch (Exception ex) {
			System.out.println(ex);
		}
	} 
}

