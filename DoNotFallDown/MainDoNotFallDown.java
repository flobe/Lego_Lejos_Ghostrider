package DoNotFallDown;

import GeneralClasses.ExchangeObject;
import GeneralClasses.MoveAround;

public class MainDoNotFallDown {

	public static void main (String [] args) {
		
		//initialising of the object needed to pass values between the Threads
		ExchangeObject ex = new ExchangeObject();
		
		//initialising the Object for the Thread checking whether there is floor or not
		DetectFloor isThereFloor = new DetectFloor(ex);
		
		//initialising the object for the Thread responsible for moving around
		MoveAround moveAround = new MoveAround(ex);
		
		//starting both Threads
		isThereFloor.start();
		moveAround.start();
	}

}
