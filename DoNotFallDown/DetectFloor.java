package DoNotFallDown;

import GeneralClasses.ExchangeObject;
import lejos.hardware.ev3.LocalEV3;
import lejos.hardware.port.Port;
import lejos.hardware.sensor.EV3ColorSensor;
import lejos.robotics.SampleProvider;

public class DetectFloor extends Thread {
	
	//initialising the objects and variables
	private ExchangeObject exObj;
	private SampleProvider sp;
	private float[] sample;
	private EV3ColorSensor colorSensor;
	//constructor
	public DetectFloor(ExchangeObject ex) {
		this.exObj = ex;
		this.exObj.setMove(true);
		
		Port secondSensorPort = LocalEV3.get().getPort("S2");
		colorSensor = new EV3ColorSensor(secondSensorPort);
		
		this.sp = colorSensor.getRedMode();
		this.sample = new float[1];
		
	}
	
	@Override
	public void run() {
		
		while (exObj.getExecute()) {
			
			sp.fetchSample(sample, 0);
			if (sample[0] == 0.0) {
				exObj.setMove(false);
				exObj.setUnderground(false);
			} else {
				exObj.setMove(true);
				exObj.setUnderground(true);
			}
		}
		colorSensor.close();
	}
	
}

