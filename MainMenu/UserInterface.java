package MainMenu;

import lejos.hardware.lcd.LCD;

public class UserInterface {
	
	//variables
	private String[] operations;
	private String[] keys = new String[3];
	private int mode;
	private int num;
	private int subNum;
	//objects
	
	
	//constructor
	UserInterface(String[] operations)  {
		this.operations = operations;
		
		this.keys[0] = "UP";
		this.keys[1] = "ENTER";
		this.keys[2] = "DOWN";

		
	}
	
	private void setMode(int m) {
		this.mode = m;
	}
	
	public int getMode() {
		return this.mode;
	}
	
	public int getNum() {
		return this.num;
	}
	
	public int getSubNum() {
		return this.subNum;
	}
	
	public void startInterface(int num) {
		LCD.clear();
		setMode(0);
		this.num = num;
		System.out.println("Ghostrider in action");
		System.out.println("ESCAPE");
		switch (num) {
		case 0:
			for (int i = 0; i < 3;  i++) {
				System.out.println(operations[i]);
			}
			break;
		case 1:
				System.out.println(operations[3]);
		}
	}
	
	public void startSubInterface(int number) {
		setMode(1);
		LCD.clear();
		this.subNum = number;
		System.out.println("Ghostrider in action");
		System.out.println("ESCAPE");
		System.out.println(operations[number] + " with cam");
		System.out.println(operations[number] + " without cam");
	}
	
}
