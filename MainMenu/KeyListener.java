package MainMenu;

import DoNotFallDown.DetectFloor;
import DoNotHitTheWall.DetectObstacle;
import GeneralClasses.Arm;
import GeneralClasses.CameraAction;
import GeneralClasses.ConnectBluetooth;
import GeneralClasses.ExchangeObject;
import GeneralClasses.Machine;
import GeneralClasses.MoveAround;
import RescueSound.RescueBot;
import lejos.hardware.Button;
import lejos.hardware.ev3.LocalEV3;
import lejos.hardware.sensor.EV3ColorSensor;
import lejos.hardware.sensor.EV3UltrasonicSensor;
import lejos.robotics.Color;
import lejos.robotics.SampleProvider;
import lejos.robotics.navigation.MovePilot;

public class KeyListener extends Thread {
	
	//initialising the variables
	
	
	//initialising the objects
	private UserInterface usrInt;
	private CameraAction ca;
	private ExchangeObject ex;
	private MoveAround moveAround;
	private DetectObstacle detObs;
	private ConnectBluetooth connBlue;
	private Arm arm;
	
	//constructor
	KeyListener(UserInterface usrInt) {
		this.usrInt = usrInt;
		this.ca = new CameraAction();
		connBlue = new ConnectBluetooth();
		arm = new Arm(connBlue);
	}
	
	//run() operation
	@Override
	public void run() {
		while (Button.ESCAPE.isUp()) {
			if (Button.UP.isDown()) {
				if (usrInt.getMode() == 0) {
					if (usrInt.getNum() == 0) {
						usrInt.startSubInterface(0);
					} else if (usrInt.getNum() == 1) {
						usrInt.startSubInterface(3);
					}
				} else if (usrInt.getMode() == 1) {
					switch (usrInt.getSubNum()) {
					case 0:
						//start arm with cam
						EV3UltrasonicSensor su = new EV3UltrasonicSensor(LocalEV3.get().getPort("S1"));
						SampleProvider sp = su.getDistanceMode();
						float[] sample = new float[1];
						ca.start();
						EV3ColorSensor cs = new EV3ColorSensor(LocalEV3.get().getPort("S2"));
						cs.getColorIDMode();
						arm.calibrate();
						Machine m = new Machine();
						MovePilot pilot = m.getPilot();
						while (Button.ENTER.isUp()) {
							if (cs.getColorID() == Color.RED) {
								sp.fetchSample(sample, 0);
								pilot.forward();
								while (sample[0] > 0.05) {
									sp.fetchSample(sample, 0);
								}
								pilot.stop();
								arm.grabSomething();
								arm.checkColor();
								break;
							}
						}
						
					case 1:
						//starte DoNotHit mit Cam
						ex = new ExchangeObject();
						moveAround = new MoveAround(ex);
						detObs = new DetectObstacle(ex, 0.20);
						arm.up();
						//starting the Threads
						moveAround.start();
						detObs.start();
						ca.start();
						break;
					case 2:
						//starte DoNotFall mit Cam
						//initialising of the object needed to pass values between the Threads
						ex = new ExchangeObject();
						arm.up();
						//initialising the Object for the Thread checking whether there is floor or not
						DetectFloor isThereFloor = new DetectFloor(ex);
						
						//initialising the object for the Thread responsible for moving around
						moveAround = new MoveAround(ex);
						
						//starting all Threads
						ca.start();
						isThereFloor.start();
						moveAround.start();
						break;
					case 3:
						//starte RescBot mit Cam
						//initialize objects
						ex = new ExchangeObject();
						connBlue = new ConnectBluetooth();
						RescueBot rescBot = new RescueBot(ex, connBlue);
						detObs = new DetectObstacle(ex, 0.2);
						
						//start the Threads
						ca.start();
						rescBot.start();
						detObs.start();
						break;
					}
				}
			} else if (Button.ENTER.isDown()) {
				if (usrInt.getMode() == 0) {
					if (usrInt.getNum() == 0) {
						usrInt.startSubInterface(1);
					}
				}
			} else if (Button.DOWN.isDown()) {
				if (usrInt.getMode() == 0) {
					if (usrInt.getNum() == 0) {
						usrInt.startSubInterface(2);
					}
				} else if (usrInt.getMode() == 1) {
					switch (usrInt.getSubNum()) {
					case 0:
						//starte Arm ohne Cam
						EV3UltrasonicSensor su = new EV3UltrasonicSensor(LocalEV3.get().getPort("S1"));
						SampleProvider sp = su.getDistanceMode();
						float[] sample = new float[1];
						EV3ColorSensor cs = new EV3ColorSensor(LocalEV3.get().getPort("S2"));
						cs.getColorIDMode();
						Machine m = new Machine();
						MovePilot pilot = m.getPilot();
						arm.calibrate();
						while (Button.ENTER.isUp()) {
							if (cs.getColorID() == Color.RED) {
								sp.fetchSample(sample, 0);
								pilot.forward();
								while (sample[0] > 0.05) {
									sp.fetchSample(sample, 0);
								}
								pilot.stop();
								arm.grabSomething();
								arm.checkColor();
								break;
							}
						}
					case 1:
						//starte DoNotHit ohne Cam
						arm.up();
						ex = new ExchangeObject();
						moveAround = new MoveAround(ex);
						detObs = new DetectObstacle(ex, 0.20);
						
						//starting the Threads
						moveAround.start();
						detObs.start();
						break;
					case 2:
						//starte DoNotFall ohne Cam
						arm.up();
						//initialising of the object needed to pass values between the Threads
						ex = new ExchangeObject();
						
						//initialising the Object for the Thread checking whether there is floor or not
						DetectFloor isThereFloor = new DetectFloor(ex);
						
						//initialising the object for the Thread responsible for moving around
						moveAround = new MoveAround(ex);
						
						//starting both Threads
						isThereFloor.start();
						moveAround.start();
						break;
					case 3:
						//starte RescBot ohne Cam
						//initialize objects
						ex = new ExchangeObject();
						connBlue = new ConnectBluetooth();
						RescueBot rescBot = new RescueBot(ex, connBlue);
						detObs = new DetectObstacle(ex, 0.2);
						
						//start the Threads
						rescBot.start();
						detObs.start();
						break;
					}
				}
			} else if (Button.LEFT.isDown()) {
				if (usrInt.getNum() == 0) {
					usrInt.startInterface(0);
				}
			} else if (Button.RIGHT.isDown()) {
				if (usrInt.getNum() == 0) {
					usrInt.startInterface(1);
				}
			}
		}
	}

}
