package MainMenu;


public class Menu {

	public static void main(String [] args) {
		
		//initilising the variables
		String[] operations = new String[4];
		operations[0] = "Arm";
		operations[1] = "DoNotHitTheWall";
		operations[2] = "DoNotFallDown";
		operations[3] = "RescueBot";
		//initialising the objects
		UserInterface userInterface = new UserInterface(operations);
		KeyListener keyListener = new KeyListener(userInterface);
		
		keyListener.start();
		userInterface.startInterface(0);
		
	}
	
}
