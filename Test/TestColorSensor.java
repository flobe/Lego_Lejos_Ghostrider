package Test;

import lejos.hardware.Button;
import lejos.hardware.ev3.LocalEV3;
import lejos.hardware.port.Port;
import lejos.hardware.sensor.EV3ColorSensor;
import lejos.robotics.SampleProvider;

public class TestColorSensor {

	public static void main(String [] args) {
		testColor();
	}

	private static void testColor() {
		Port sensorPort = LocalEV3.get().getPort("S2");
		EV3ColorSensor cs = new EV3ColorSensor(sensorPort);
		while (Button.ESCAPE.isUp()) {
			System.out.println(cs.getColorID());
		}
		cs.close();
	}
	
	private static void testRed() {
		Port sensorPort = LocalEV3.get().getPort("S2");
		EV3ColorSensor cs = new EV3ColorSensor(sensorPort);
		SampleProvider sp = cs.getRedMode();
		float[] sample = new float[1];
		while (Button.ESCAPE.isUp()) {
			sp.fetchSample(sample, 0);
			System.out.println(sample[0]);
		}
		cs.close();
	}
}


//on line		0.05
//half of Line	0.19
//out of line	0.35
//difference	~0.05-0.
