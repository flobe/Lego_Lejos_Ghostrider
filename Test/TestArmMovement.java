package Test;

import GeneralClasses.Arm;
import GeneralClasses.ConnectBluetooth;

public class TestArmMovement {

	
	public static void main(String[] args) {
		ConnectBluetooth connBlue = new ConnectBluetooth();
		Arm arm = new Arm(connBlue);
		arm.calibrate();
		arm.grabSomething();
		arm.checkColor();
		connBlue.closeMotor("remoteA");
		connBlue.closeMotor("remoteB");
		connBlue.closeMotor("remoteC");
		connBlue.closeMotor("remoteD");
	}
}
