package Test;

import lejos.hardware.Button;
import lejos.hardware.ev3.LocalEV3;
import lejos.hardware.port.Port;
import lejos.hardware.sensor.NXTSoundSensor;
import lejos.robotics.SampleProvider;

public class TestShowSoundSensorValues {

	
	public static void main(String [] args) {
		
		
		Port sensorPort4 = LocalEV3.get().getPort("S4");
		NXTSoundSensor soundSensor1 = new NXTSoundSensor(sensorPort4);
		SampleProvider sp1 = soundSensor1.getDBMode();
		float[] sample1 = new float[1];
		
		Port sensorPort2 = LocalEV3.get().getPort("S2");
		NXTSoundSensor soundSensor2 = new NXTSoundSensor(sensorPort2);
		SampleProvider sp2 = soundSensor2.getDBMode();
		float[] sample2 = new float[1];
		
		Port sensorPort3 = LocalEV3.get().getPort("S3");
		NXTSoundSensor soundSensor3 = new NXTSoundSensor(sensorPort3);
		SampleProvider sp3 = soundSensor3.getDBMode();
		float[] sample3 = new float[1];
		
		while(Button.ESCAPE.isUp()) {
			
			sp1.fetchSample(sample1, 0);
			sp2.fetchSample(sample2, 0);
			sp3.fetchSample(sample3, 0);
			System.out.println(sample1[0]);
		}
		soundSensor1.close();
		soundSensor2.close();
		soundSensor3.close();
	}
}

