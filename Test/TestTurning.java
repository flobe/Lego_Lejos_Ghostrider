package Test;

import GeneralClasses.Machine;
import lejos.robotics.navigation.MovePilot;

public class TestTurning {
	
	
	public static void main(String [] args) {
		
		Machine m = new Machine();
		MovePilot pilot = m.getPilot();
		
		pilot.rotate(20);
	}
}
