package Test;

import GeneralClasses.Machine;
import lejos.hardware.Button;
import lejos.robotics.navigation.MovePilot;
import lejos.utility.Delay;

public class TestForwardOperation {

	public static void main(String [] args) {
		
		Machine m = new Machine();
		MovePilot pilot = m.getPilot();
		
		while (Button.ESCAPE.isUp()) {
			pilot.forward();
			Delay.msDelay(2000);
		}
		
	}
	
}
