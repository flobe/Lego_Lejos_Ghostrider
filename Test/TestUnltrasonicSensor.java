package Test;

import lejos.hardware.Button;
import lejos.hardware.ev3.LocalEV3;
import lejos.hardware.port.Port;
import lejos.hardware.sensor.EV3UltrasonicSensor;
import lejos.robotics.SampleProvider;

public class TestUnltrasonicSensor {
	
	
	public static void main(String [] args) {
		
		Port sensorPort = LocalEV3.get().getPort("S1");
		EV3UltrasonicSensor ultrasonicSensor = new EV3UltrasonicSensor(sensorPort);
		
		SampleProvider sp = ultrasonicSensor.getDistanceMode();
		float[] sample = new float[1];
		while (Button.DOWN.isUp()) {
			sp.fetchSample(sample, 0);
			System.out.println((double)sample[0]);
		}
		ultrasonicSensor.close();
	}
}
