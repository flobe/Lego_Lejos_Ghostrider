package Test;

import lejos.hardware.ev3.LocalEV3;
import lejos.hardware.motor.EV3LargeRegulatedMotor;
import lejos.robotics.chassis.Chassis;
import lejos.robotics.chassis.Wheel;
import lejos.robotics.chassis.WheeledChassis;
import lejos.robotics.navigation.MovePilot;
import lejos.utility.Delay;

public class TestMovementForFollowTheLine {

	
	public static void main(String [] args) {
		EV3LargeRegulatedMotor leftMotorB = new EV3LargeRegulatedMotor(LocalEV3.get().getPort("B"));
		EV3LargeRegulatedMotor rightMotorC = new EV3LargeRegulatedMotor(LocalEV3.get().getPort("C"));
		Wheel leftWheel = WheeledChassis.modelWheel(leftMotorB, 56.0).offset(97.5);
		Wheel rightWheel = WheeledChassis.modelWheel(rightMotorC, 56.0).offset(-97.5);
		Chassis myChassis = new WheeledChassis( new Wheel[]{leftWheel, rightWheel}, WheeledChassis.TYPE_DIFFERENTIAL);
		MovePilot pilot = new MovePilot(myChassis);
		
		pilot.setLinearSpeed(100);
//		myChassis.setVelocity(100, 100);
		pilot.forward();
		Delay.msDelay(3000);
		
		
		
	}
}
