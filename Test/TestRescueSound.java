package Test;

import GeneralClasses.ConnectBluetooth;
import GeneralClasses.ExchangeObject;
import RescueSound.RescueBot;

public class TestRescueSound {

	
	public static void main(String[] args) {
		ConnectBluetooth bluetoothRobot = new ConnectBluetooth();
		ExchangeObject ex = new ExchangeObject();
		RescueBot rescBot = new RescueBot(ex,bluetoothRobot);
		rescBot.start();
	}
}
