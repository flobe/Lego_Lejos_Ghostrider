package Test;

import lejos.hardware.ev3.LocalEV3;
import lejos.hardware.motor.Motor;
import lejos.hardware.motor.UnregulatedMotor;
import lejos.robotics.RegulatedMotor;
import lejos.robotics.chassis.Chassis;
import lejos.robotics.chassis.Wheel;
import lejos.robotics.chassis.WheeledChassis;
import lejos.robotics.navigation.MovePilot;
import lejos.utility.Delay;

public class TestUnregulatedMotor {

	
	
	public static void main(String [] args) {
		
		MovePilot pilot;
		UnregulatedMotor unregLeft;
		UnregulatedMotor unregRight;
		RegulatedMotor regLeft;
		RegulatedMotor regRight;
		
		
		regLeft = Motor.B;
		regRight = Motor.C;
		
		unregLeft = new UnregulatedMotor(LocalEV3.get().getPort("B"));
		unregRight = new UnregulatedMotor(LocalEV3.get().getPort("C"));
		
		Wheel leftWheel = WheeledChassis.modelWheel(regLeft, 56.0).offset(97.5);
		Wheel rightWheel = WheeledChassis.modelWheel(regRight, 56.0).offset(-97.5);
		Chassis myChassis = new WheeledChassis( new Wheel[]{leftWheel, rightWheel}, WheeledChassis.TYPE_DIFFERENTIAL);
		pilot = new MovePilot(myChassis);
		
		
		
		pilot.forward();
		Delay.msDelay(200);
		unregLeft.setPower(40);
		Delay.msDelay(200);
		unregRight.setPower(40);
		
		unregLeft.close();
		unregRight.close();
	}
	
	//Problem:
//	Executing jrun -cp /home/lejos/programs/Test.jar lejos.internal.ev3.EV3Wrapper Test.Test in /home/lejos/programs
//	lejos.hardware.DeviceException: unable to open port
//		at lejos.internal.ev3.EV3Port.open(EV3Port.java:75)
//		at lejos.hardware.motor.UnregulatedMotor.<init>(UnregulatedMotor.java:23)
//		at lejos.hardware.motor.UnregulatedMotor.<init>(UnregulatedMotor.java:34)
//		at Test.Test.main(Test.java:29)
//		at sun.reflect.NativeMethodAccessorImpl.invoke0(Native Method)
//		at sun.reflect.NativeMethodAccessorImpl.invoke(NativeMethodAccessorImpl.java:57)
//		at sun.reflect.DelegatingMethodAccessorImpl.invoke(DelegatingMethodAccessorImpl.java:43)
//		at java.lang.reflect.Method.invoke(Method.java:606)
//		at lejos.internal.ev3.EV3Wrapper.invokeClass(EV3Wrapper.java:62)
//		at lejos.internal.ev3.EV3Wrapper.main(EV3Wrapper.java:46)

	
}
