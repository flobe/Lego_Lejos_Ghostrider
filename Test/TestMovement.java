package Test;

import lejos.hardware.motor.Motor;
import lejos.utility.Delay;

public class TestMovement {

	
	public static void main(String [] args) {
		
		
		Motor.B.setSpeed(100);
		Motor.C.setSpeed(100);
		Motor.B.forward();
		Motor.C.forward();
		Delay.msDelay(3000);
	}
}
