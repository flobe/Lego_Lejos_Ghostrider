package ArmAction;

import GeneralClasses.Arm;
import GeneralClasses.ConnectBluetooth;
import lejos.hardware.Button;
import lejos.hardware.ev3.LocalEV3;
import lejos.hardware.sensor.EV3ColorSensor;
import lejos.hardware.sensor.EV3UltrasonicSensor;
import lejos.robotics.Color;
import lejos.robotics.SampleProvider;

public class ArmMain {

	
	public static void main(String[] args) {
		
		//initialising the variables
		
		//initialising the objects
		ConnectBluetooth connBlue = new ConnectBluetooth();
		Arm arm = new Arm(connBlue);
		EV3ColorSensor cs = new EV3ColorSensor(LocalEV3.get().getPort("S2"));
		EV3UltrasonicSensor su = new EV3UltrasonicSensor(LocalEV3.get().getPort("S3"));
		SampleProvider sp = su.getDistanceMode();
		float[] sample = new float[1];
		cs.getColorIDMode();
		arm.calibrate();
		while (Button.ENTER.isUp()) {
			if (cs.getColorID() == Color.RED) {
				sp.fetchSample(sample, 0);
				while (sample[0] > 0.05) {
					sp.fetchSample(sample, 0);
				}
				arm.grabSomething();
				arm.checkColor();
			}
		}
		cs.close();
		su.close();
		
	}
}
